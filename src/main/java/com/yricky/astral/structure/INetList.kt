package com.yricky.astral.structure

import com.yricky.astral.lib.VerilogKeyword
import com.yricky.astral.project.Hazuki
import com.yricky.astral.signal.AssignableSignal
import com.yricky.astral.signal.Input
import com.yricky.astral.signal.Output

interface INetList:Hazuki.Checkable {
    val iPorts:Map<String, Input>
    val oPorts:Map<String, Output>
    val netNodes:Map<String, AssignableSignal>
    val name:String

    fun checkNameValidity(name:String){
        when(name){
            in VerilogKeyword.set ->{
                throw IllegalArgumentException("Name \"$name\" is a verilog keyword!")
            }
            in netNodes.keys -> {
                throw IllegalArgumentException("Signal \"$name\" has been declared!")
            }
        }
    }
}

