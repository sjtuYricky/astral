package com.yricky.astral.structure

import com.yricky.astral.signal.Wire

class ClkRstPair (
    val clock: Wire,
    val reset: Wire
){
    init {
        if((clock.width!=1) || (reset.width!=1))
            throw IllegalArgumentException("Width of timing signal must be 1!")
    }
}