package com.yricky.astral.structure.scope

import com.yricky.astral.signal.Wire
import com.yricky.astral.structure.ClkRstPair
import com.yricky.astral.structure.INetList

interface ModuleScopeBasic:
    SignalUtils
{
    val clkRstPair: ClkRstPair?

    fun inputWire(name:String,width:Int = 1): Wire
    fun wire(name:String,width:Int = 1,tags:Collection<String>? = null): Wire
    fun outputWire(name:String,width:Int = 1): Wire

    fun getModuleName():String = this::class.java.name.split(".").last()

    fun genNetList(): INetList
}