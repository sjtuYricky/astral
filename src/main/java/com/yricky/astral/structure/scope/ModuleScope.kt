package com.yricky.astral.structure.scope

import com.yricky.astral.signal.Reg
import com.yricky.astral.structure.ClkRstPair

interface ModuleScope:
    ModuleScopeBasic,
    SignalLogic,
    SignalLogicNotGood
{
    fun reg(name:String,width:Int = 1,timing: ClkRstPair? = clkRstPair): Reg
    fun outputReg(name:String,width:Int = 1,timing: ClkRstPair? = clkRstPair): Reg
}