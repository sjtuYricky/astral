package com.yricky.astral.structure

import com.yricky.astral.lib.VerilogKeyword
import com.yricky.astral.signal.*

abstract class MutableNetList(override val name: String) :
    INetList
{
    override val iPorts: MutableMap<String, Input> = LinkedHashMap()
    override val oPorts: MutableMap<String, Output> = LinkedHashMap()
    override val netNodes: MutableMap<String, AssignableSignal> = LinkedHashMap()

    fun addI(s: Wire) {
        iPorts[s.name] = object :Input,AssignableSignal by s{}
    }
    fun addO(s:AssignableSignal) {
        oPorts[s.name] = object :Output,AssignableSignal by s{
            override val type: String = if (s is Reg) VerilogKeyword.REG else VerilogKeyword.WIRE
        }
    }
}