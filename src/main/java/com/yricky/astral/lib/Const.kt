package com.yricky.astral.lib


object Const {
    const val HEX = 16
    const val BIN = 2
    const val DEC = 10
    val RADIX_CHAR = mapOf(
        Pair(HEX,'h'),
        Pair(BIN,'b'),
        Pair(DEC,'d'),
    )
    val BIT_MASK = longArrayOf(
        0L,
        0x0000000000000001L,
        0x0000000000000003L,
        0x0000000000000007L,
        0x000000000000000fL,
        0x000000000000001fL,
        0x000000000000003fL,
        0x000000000000007fL,
        0x00000000000000ffL,
        0x00000000000001ffL,
        0x00000000000003ffL,
        0x00000000000007ffL,
        0x0000000000000fffL,
        0x0000000000001fffL,
        0x0000000000003fffL,
        0x0000000000007fffL,
        0x000000000000ffffL,
        0x000000000001ffffL,
        0x000000000003ffffL,
        0x000000000007ffffL,
        0x00000000000fffffL,
        0x00000000001fffffL,
        0x00000000003fffffL,
        0x00000000007fffffL,
        0x0000000000ffffffL,
        0x0000000001ffffffL,
        0x0000000003ffffffL,
        0x0000000007ffffffL,
        0x000000000fffffffL,
        0x000000001fffffffL,
        0x000000003fffffffL,
        0x000000007fffffffL,
        0x00000000ffffffffL,
        0x00000001ffffffffL,
        0x00000003ffffffffL,
        0x00000007ffffffffL,
        0x0000000fffffffffL,
        0x0000001fffffffffL,
        0x0000003fffffffffL,
        0x0000007fffffffffL,
        0x000000ffffffffffL,
        0x000001ffffffffffL,
        0x000003ffffffffffL,
        0x000007ffffffffffL,
        0x00000fffffffffffL,
        0x00001fffffffffffL,
        0x00003fffffffffffL,
        0x00007fffffffffffL,
        0x0000ffffffffffffL,
        0x0001ffffffffffffL,
        0x0003ffffffffffffL,
        0x0007ffffffffffffL,
        0x000fffffffffffffL,
        0x001fffffffffffffL,
        0x003fffffffffffffL,
        0x007fffffffffffffL,
        0x00ffffffffffffffL,
        0x01ffffffffffffffL,
        0x03ffffffffffffffL,
        0x07ffffffffffffffL,
        0x0fffffffffffffffL,
        0x1fffffffffffffffL,
        0x3fffffffffffffffL,
        0x7fffffffffffffffL,
        -0x1L
    ).asList()
}

object VerilogKeyword{
    val set:Set<String> by lazy{
        setOf(
            MODULE,
            ENDMODULE,
            BEGIN,
            END,
            ALWAYS,
            ASSIGN,
            INPUT,
            OUTPUT,
            POSEDGE,
            NEGEDGE,
            IF,
            ELSE
        )
    }
    const val MODULE = "module"
    const val ENDMODULE = "endmodule"
    const val BEGIN = "begin"
    const val END = "end"
    const val ALWAYS = "always"
    const val WIRE = "wire"
    const val REG = "reg"
    const val ASSIGN = "assign"
    const val INPUT = "input"
    const val OUTPUT = "output"
    const val POSEDGE = "posedge"
    const val NEGEDGE = "negedge"
    const val IF = "if"
    const val ELSE = "else"
}