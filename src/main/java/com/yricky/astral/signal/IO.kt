package com.yricky.astral.signal
import com.yricky.astral.lib.VerilogKeyword.INPUT
import com.yricky.astral.lib.VerilogKeyword.OUTPUT
import com.yricky.astral.lib.VerilogKeyword.WIRE
import com.yricky.astral.structure.AstralModule
interface IO:AssignableSignal {
    fun generateDeclareIO(): String
}

/**
 * Don't use this directly!
 * @see AstralModule.inputWire
 */
interface Input:IO {
    override fun generateDeclareIO(): String {
        val widthInfo = if(width>1) "[${width-1}:0]" else ""
        return "$INPUT $WIRE$widthInfo $name"
    }
}

/**
 * Don't use this directly!
 * @see AstralModule.outputReg
 * @see AstralModule.outputWire
 */
interface Output:IO {
    val type:String
    override fun generateDeclareIO(): String {
        val widthInfo = if(width>1) "[${width-1}:0]" else ""
        return "$OUTPUT $type$widthInfo $name"
    }
}