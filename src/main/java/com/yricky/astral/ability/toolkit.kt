package com.yricky.astral.ability

import com.yricky.astral.signal.Signal
import kotlin.random.Random

fun AbilityModule.Counter(name:String = "reg_cnt_${Random.nextInt()}",width:Int = 32,step:Int = 1):Signal{
    return reg(name,width,clkRstPair).also {
        it assignTo (it add step.w())
    }
}