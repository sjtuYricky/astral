package com.yricky.astral.ability

import com.yricky.astral.signal.Signal
import com.yricky.astral.structure.ClkRstPair
import com.yricky.astral.structure.scope.ModuleScope
import com.yricky.astral.structure.scope.ModuleScopeBasic

interface AbilityModuleScope:ModuleScope,AbilityModuleScopeBasic,TimingScope{
    fun attach(str:()->String)
}
interface ImplModuleScope:AbilityModuleScopeBasic{

}



interface AbilityModuleScopeBasic:ModuleScopeBasic
interface TimingScope{
    val clkRstPairStack:MutableList<ClkRstPair>
    val clock: Signal
    val reset:Signal
}