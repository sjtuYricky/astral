package com.yricky.astral.ability

import com.yricky.astral.compile.generate
import com.yricky.astral.structure.AstralModule
import java.io.File


fun AstralModule.writeToFileInDir(dirName:String? = null,type:String = ""){
    if(dirName.isNullOrEmpty()){
        File("${getModuleName()}.v").writeText(generate())
    }else{
        File(dirName).also {
            if(!it.exists())
                it.mkdirs()
            File(it,"${getModuleName()}.v").writeText(generate())
        }
    }
}