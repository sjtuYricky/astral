package com.yricky.astral.ability

import com.yricky.astral.signal.Signal
import com.yricky.astral.structure.ClkRstPair
import com.yricky.astral.structure.AstralModule

abstract class AbilityModule(private val name:String):
    AstralModule(),
    AbilityModuleScope
{
    override val clkRstPairStack:MutableList<ClkRstPair> = ArrayList()
    override val clkRstPair: ClkRstPair
        get() = clkRstPairStack.lastOrNull() ?: throw RuntimeException("Not in a timing area!")
    override val clock:Signal get(){
        return clkRstPair.clock
    }

    override val reset:Signal get(){
        return clkRstPair.reset
    }

    override fun getModuleName(): String {
        return name
    }

    override fun attach(str:()->String){
        _attachments.add(str)
    }

}