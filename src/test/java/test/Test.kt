package test

import com.yricky.astral.ability.*
import java.io.File

fun main(){
    val dir = File("artifacts")
    if(!dir.isDirectory){
        dir.delete()
        dir.mkdirs()
    }

    ImplModule("impl"){

    }

    AAModule("and"){
        Timing(InputWire("clk1"),InputWire("rst_n1")){
            OutputReg("out1"){
                InputWire("in11") and InputWire("in12")
            }
        }
        Timing(InputWire("clk2"),InputWire("rst_n2")){
            OutputReg("out2"){
                InputWire("in21") and InputWire("in22")
            }
        }
    }.writeToFileInDir("artifacts")




    AAModule("ctrl"){
        Timing("clk","rst"){
            OutputWire("led",4){ led ->
                InputWire("data",4).let{ data->
                    joint(
                        data.split(3) eq 0.w(1),
                        data.split(3,2) eq 0b01.w(2),
                        data.split(3,1) eq 0b010.w(3),
                        data.split(3,0) eq 0b0100.w(4)
                    )
                }
            }
        }
    }.writeToFileInDir("artifacts")

}